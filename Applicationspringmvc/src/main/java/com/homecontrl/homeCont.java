package com.homecontrl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entities.Todo;

@Controller
public class homeCont {

	@RequestMapping("/homet")
	public String home(Model m)
	{
		String str="index";
		m.addAttribute("page", str);
		return "index";
	}
	
	@RequestMapping("/add")
	public String addTODO(Model m)
	{
		Todo t=new Todo();
		m.addAttribute("page", "add");
		m.addAttribute("todo", t);
		return "index";
	}
	
	@RequestMapping(value="/saveTodo", method=RequestMethod.POST)
	public String saveTodo(@ModelAttribute("todo") Todo t, Model m)
	{
		System.out.println(t);
		return "index";
	}
	
	}
