<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    <%@page isELIgnored="false" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    
<!DOCTYPE html>
!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>c:out value="${page }"</title>//name of page is shown in url..
  </head>
  <body>
    <div class="container mt-2">
    <h1 class-"text-center">Welcome to Applicationspringmvc manage</h1>
    <div class="row">
    <div class="row mt-5">
    
    <div class="col-md-2">
  <!-- <h3 class="text-center">Options</h3> -->
  
   <div class="list-group">
  <button type="button"
   class="list-group-item list-group-item-action active"> Menu</button>
  <a href='<c:url value='/add'></c:url>'
   class="list-group-item list-group-item-action">Add TODO</a>
   
  <a href='<c:url value='/homet'></c:url>'
   class="list-group-item list-group-item-action">View TODO</a>
  
  </div>
    </div>
    
    <div class="col-md-10">
      <c:if test="${page=='index' }">
         <h1 class="text-center">All TODOs</h1>
      </c:if>
      
      <c:if test="${page=='add' }">
         <h1 class="text-center">Add TODO</h1>
      </c:if>
      <br>
      <form:form action="saveTodo" method="post" modelAttribute="todo">
      
      <div class="form-group">
          <form:input path="todoTitle" cssClass="form-control" placeholder="Enter your todo title" />
      </div>
      
      <div class="form-group">
         <form:textarea path="todoContent" cssClass="form-control" placeholder="Enter your todo content" cssStyle="height:200px"/>
      </div>
      
      <div class="container text-center">
      <button class="btn btn-outline-success">Add Todo</button>
      </div>
      
      </form:form>
      
      
    </div>
        
    
    
    
  </body>
</html>