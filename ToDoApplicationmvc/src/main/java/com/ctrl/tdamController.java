package com.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class tdamController {

	@RequestMapping("/india")
	public String IndiaWelcoming(Model model)
	{
		System.out.println("this is india url for india view");
		model.addAttribute("name","Swapnali Patil");
		model.addAttribute("id",4356);
		
		return "india";
	}
	@RequestMapping("/about")
	public String HomeInIndia()
	{
		return "about";
	}
	@RequestMapping("/help")
	public ModelAndView Help()
	{
		System.out.println("This is help controller");
		
		//createing object of modelAndView
		ModelAndView modelAndView=new ModelAndView();
		
		//setting the data
		modelAndView.addObject("name2", "Sonal patil");
		modelAndView.addObject("name", "priti");
		modelAndView.addObject("rollno", 567);
		modelAndView.addObject("lastname", "swapnali");

        //set view name
		modelAndView.setViewName("Help");
		
		return modelAndView;
	}
	
	
}
